from django.db import models

class Status(models.Model):
    name = models.CharField(max_length=100)
    message = models.CharField(max_length=280)

    def __str__(self):
        return self.name + ": " + self.message