from django import forms
from .models import Status


class StatusForm(forms.ModelForm):

    class Meta:
        model = Status
        fields = ('name', 'message')
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'id': 'name_field',
                'placeholder': 'Name',
                }),
            'message': forms.TextInput(attrs={
                'class': 'form-control',
                'id': 'message_field',
                'placeholder': 'Message'
                }),
        }
