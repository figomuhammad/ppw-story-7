from django.shortcuts import render, redirect
from .models import *
from .forms import *

name = None
message = None

def index(request):
    global name, message
    
    statuses = Status.objects.all()
    statusForm = StatusForm(request.POST or None)

    if request.method == 'POST' and statusForm.is_valid():
        name = statusForm.cleaned_data.get('name')
        message = statusForm.cleaned_data.get('message')
        return redirect('/confirmation')

    return render(request, 'index.html', {'statuses': statuses, 'status_form': statusForm})

def confirmation(request):
    global name, message
    if request.method == 'POST':
        Status.objects.create(name=name, message=message)
        return redirect('/')

    return render(request, 'confirmation.html', {'name': name, 'message': message})
