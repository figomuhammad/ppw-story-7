from django.test import TestCase
from django.urls import resolve
from .models import *
from .views import *
from .forms import *
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys


class UnitTest(TestCase):
    
    # Index Page
    # Template
    def testIndexPageIsExist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def testIndexPageUsesIndexHTML(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')

    def testIndexPageUsingIndexFunc(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def testIndexPageContainsHelloHowAreYou(self):
        response = self.client.get('/')
        self.assertIn("Hello, how are you?", response.content.decode('utf8'))

    def testIndexPageHasForm(self):
        response = self.client.get('/')
        self.assertIn("<form", response.content.decode('utf8'))
        self.assertIn("</form>", response.content.decode('utf8'))
        self.assertIsInstance(response.context['status_form'], StatusForm)

    def testIndexPageHasStatusFormField(self):
        response = self.client.get('/')
        self.assertIn("<input", response.content.decode('utf8'))

    def testIndexPageHasSubmitButton(self):
        response = self.client.get('/')
        self.assertIn('<button type="submit"', response.content.decode('utf8'))
        self.assertIn("</button>", response.content.decode('utf8'))

    def testIndexPageHasStatusList(self):
        response = self.client.get('/')
        self.assertIn('<ul class="list-group', response.content.decode('utf8'))
        self.assertIn("</ul>", response.content.decode('utf8'))

    # Confirmation Page
    # Template
    def testConfirmationPageIsExist(self):
        response = self.client.get('/confirmation')
        self.assertEqual(response.status_code, 200)

    def testConfirmationPageUsesConfirmationHTML(self):
        response = self.client.get('/confirmation')
        self.assertTemplateUsed(response, 'confirmation.html')

    def testConfirmationPageUsingConfirmationFunc(self):
        found = resolve('/confirmation')
        self.assertEqual(found.func, confirmation)

    def testConfirmationPageHasSubmitButton(self):
        response = self.client.get('/confirmation')
        self.assertIn('type="submit"', response.content.decode('utf8'))
        self.assertIn("</button>", response.content.decode('utf8'))
    
    def testConfirmationPageHasCancelButton(self):
        response = self.client.get('/confirmation')
        self.assertIn('<a class="btn', response.content.decode('utf8'))
        self.assertIn('href="/"', response.content.decode('utf8'))

    # Model
    def testModelStatusCanCreateObject(self):
        Status.objects.create(name='Name', message='Test')
        count = Status.objects.all().count()
        self.assertEqual(count, 1)

    def testModelStatusReturnMessageAttribute(self):
        obj = Status.objects.create(name='Name', message='Test')
        self.assertEqual(str(obj),'Name: Test')


class FunctionalTest(TestCase):
    browser = None
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def testMainFeature(self):
        self.browser.get("http://127.0.0.1:8000")

        nameField = self.browser.find_element_by_id("name_field")
        statusField = self.browser.find_element_by_id("message_field")

        nameField.send_keys("Name")
        statusField.send_keys("Status")
        statusField.submit()

        self.assertIn("Name", self.browser.page_source)
        self.assertIn("Status", self.browser.page_source)

        confirmationButton = self.browser.find_element_by_id("confirmation_button")
        confirmationButton.submit()

        self.assertIn("Name", self.browser.page_source)
        self.assertIn("Status", self.browser.page_source)
