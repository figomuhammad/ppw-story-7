from django.apps import AppConfig


class S7AppConfig(AppConfig):
    name = 's7app'
